# wichtelbaustelle

Nichtkommerzielles OpenSource Design für eine Wichtelbaustelle. Eine Wichtelbaustelle ist ein moderner Adventsbrauch, bei dem eine Miniaturdarstellung einer Baustelle gebastelt wird. Kinder finden in dieser "Baustelle" jeden Tag kleine Geschenke wie Süßigkeiten oder Spielzeug.

In diesem Repo enthalten sind SVG Dateien zum ausdrucken, OpenSCAD Modelle für 3D Druck und Anleitungen zum Nachbau von Modellen.

Im Fediverse sind weitere Informationen unter dem Hashtag #wichtelbaustelle2024 und #wichtelbaustelle zu finden. Zunächst werden in den Issues Ideen gesammelt, du darfst gerne etwas beisteuern! :-)

Jeglicher content in diesem Repo wird von der Community eigens erstellt und steht unter nichtkommerzieller CC Lizenz.